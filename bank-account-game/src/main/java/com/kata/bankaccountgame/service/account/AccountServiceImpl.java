package com.kata.bankaccountgame.service.account;

import com.kata.bankaccountgame.data.Account;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService{
    List<Account> accountList = new ArrayList<>();

    @Override
    public List<Account> findAll() {
        return null;
    }

    @Override
    public void save() {

    }

    @Override
    public Account buildNewAccount(String accountNumber, Long accountBalance) {
        Account account = Account.builder()
                .number(accountNumber)
                .balance(accountBalance)
                .build();
        accountList.add(account);

        return account;
    }

    @Override
    public boolean deposite(String accountNumber, Long accountBalance) {
        boolean isDepositeOk = false;
        Optional<Account> foundAccount = accountList.stream()
                .filter(account -> account.getNumber().equals(accountNumber))
                .findFirst();

        if (foundAccount.isPresent()) {
            foundAccount.get().setBalance(foundAccount.get().getBalance() + accountBalance);
            isDepositeOk = true;
        }
        return isDepositeOk;
    }
}

package com.kata.bankaccountgame.service.account;

import com.kata.bankaccountgame.data.Account;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface AccountService {
    List<Account> findAll();
    void save();
    Account buildNewAccount(String accountNumber, Long accountBalance);
    boolean deposite(String accountNumber, Long accountBalance);
}

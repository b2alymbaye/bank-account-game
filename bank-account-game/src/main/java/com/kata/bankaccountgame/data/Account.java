package com.kata.bankaccountgame.data;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Account {
    private String number;
    private Long balance;
}

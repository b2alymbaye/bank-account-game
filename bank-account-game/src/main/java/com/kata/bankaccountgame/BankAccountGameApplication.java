package com.kata.bankaccountgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankAccountGameApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankAccountGameApplication.class, args);
    }

}

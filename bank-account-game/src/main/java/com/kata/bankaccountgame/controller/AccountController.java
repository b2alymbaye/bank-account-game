package com.kata.bankaccountgame.controller;


import com.kata.bankaccountgame.service.account.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
public class AccountController {
    @Autowired
    private final AccountService accountService;

    @PostMapping("/accounts/{accountNumber}/{accountBalance}/create")
    void buildNewAccount(@PathVariable String accountNumber, @PathVariable Long accountBalance) {
        accountService.buildNewAccount(accountNumber, accountBalance);
    }

    @PostMapping("/accounts/{accountNumber}/deposit")
    void deposit(@PathVariable String accountNumber, @PathVariable Long accountBalance) {
        accountService.deposite(accountNumber, accountBalance);
    }
}

package com.kata.bankaccountgame.data;

import com.kata.bankaccountgame.service.account.AccountServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AccountTest {
    @Mock
    private AccountServiceImpl accountService;

    @Test
    void contextLoads() {
    }

    @Test
    public void givenAccountNumber_WhenAccountExist_thenReturnAccountBalance() {
        String accountNumber = "#123MERGUEZ";
        Long accountBalance = 2000L;
        Account account = new Account(accountNumber, accountBalance);

        assertEquals(accountBalance, account.getBalance());
    }

}

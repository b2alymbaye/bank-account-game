package com.kata.bankaccountgame.service.account;

import com.kata.bankaccountgame.data.Account;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.awaitility.Awaitility.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
class AccountServiceTest {
    @Mock
    private AccountServiceImpl accountService;

    @Test
    void contextLoads() {
    }

    @Test
    public void givenAccountInformation_WhenBuildNewAccount_thenReturnNewAccount() {
        // Given
        String accountNumber = "#123MERGUEZ";
        Long accountBalance = 2000L;
        Account accountResult = new Account(accountNumber, accountBalance);

        // When
        when(accountService.buildNewAccount(accountNumber, accountBalance)).thenReturn(accountResult);

        // Then
        assertEquals(accountResult.getNumber(), accountService.buildNewAccount(accountNumber, accountBalance).getNumber());
    }


    @Test
    public void givenAccountInformationAndBalance_WhenDeposite_thenReturnTrueIfDepositeOk() {
        // Given
        String accountNumber = "#123MERGUEZ";
        Long accountBalance = 2000L;
        Long depositeToAccountBalance = 10L;
        Account existingAccount = new Account(accountNumber, accountBalance);

        Account accountResult = new Account(accountNumber, accountBalance + depositeToAccountBalance);

        // When
        when(accountService.deposite(accountNumber, depositeToAccountBalance)).thenReturn(true);

        // Then
        assertTrue(accountService.deposite(accountNumber, depositeToAccountBalance));
    }
}
